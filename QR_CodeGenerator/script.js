let img = document.getElementById("img");
let qrImage = document.getElementById("qrImage");
let qrText = document.getElementById("qrText");

function generateQR() {
  
  if (qrText.value == "") {
    alert("Please Enter Something to Generate QR Code")
  }
  else{
    qrImage.src =
    "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" +
    qrText.value;
    img.classList.add("show-img")
  }
  
}
